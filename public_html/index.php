<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

include_once ( 'php/common.php' ) ;

$db = openDB ( 'wikidata' , 'wikimedia' ) ;
$projects = array ( 'wiki' => 'wikipedia' ) ;

$lang = array() ;
$proj = array() ;
$lang[1] = get_request ( 'lang1' , 'de' ) ;
$lang[2] = get_request ( 'lang2' , 'en' ) ;
$proj[1] = get_request ( 'proj1' , 'wiki' ) ;
$proj[2] = get_request ( 'proj2' , 'wiki' ) ;

$start = get_request ( 'start' , 0 ) ;
$limit = get_request ( 'limit' , 100 ) ;

$cat = get_request ( 'cat' , '' ) ;
$starts_with = get_request ( 'starts_with' , '' ) ;
$depth = get_request ( 'depth' , '9' ) ;
$doit = isset ( $_REQUEST['doit'] ) ;

print get_common_header ( '' , 'Not in the other language' ) ;
print "<p>This tool looks for Wikidata items that have a page in one language but not in the other.</p>" ;
print "<form method='get' class='form-inline'>" ;
print "<table class='table table-condensed'><tbody>" ;

for ( $i = 1 ; $i <= 2 ; $i++ ) {
	print "<tr><th nowrap>" ;
	print ( $i == 1 ? 'In' : 'Not in' ) ;
	print " language</td><td style='width:100%'><input class='span2' type='text' name='lang$i' value='" . $lang[$i] . "' /> . <select name='proj$i' class='span2'>" ;
	foreach ( $projects AS $k => $v ) {
		print "<option value='$k'" ;
		if ( $k == $proj[$i] ) print " selected" ;
		print ">$v</option>" ;
	}
	print "</select></td></tr>" ;
}

print "<tr><th>Category tree</th><td><input class='span4' type='text' name='cat' value='$cat' />, depth <input class='span1' name='depth' type='text' value='$depth' /> (optional)</td></tr>" ;
print "<tr><th>Page title</th><td>starts with <input class='span4' type='text' name='starts_with' value='$starts_with' /> (optional)</td></tr>" ;
print "<tr><td/><td><input type='submit' name='doit' value='Do it'/></td></tr>" ;
print "</tbody></table>" ;
print "</form>" ;

function get_content_lang($lang)
{
	$content_lang_map = array(
		'als' => 'gsw',
		'bat-smg' => 'sgs',
		'be-x-old' => 'be-tarask',
		'bh' => 'bho',
		'crh' => 'chr-latn',
		'fiu-vro' => 'vro',
		'no' => 'nb',
		'roa-rup' => 'rup',
		'simple' => 'en',
		'zh-classical' => 'lzh',
		'zh-min-nan' => 'nan',
		'zh-yue' => 'yue',
	);
	return isset($content_lang_map[$lang]) ? $content_lang_map[$lang] : $lang;
}

if ( $doit ) {

	print "<hr/>" ;
	$k1 = get_db_safe ( $lang[1].$proj[1] ) ;
	$k2 = get_db_safe ( $lang[2].$proj[2] ) ;
	
	$sql = "SELECT i1.ips_item_id from wb_items_per_site i1" ;
	$sql .= " LEFT OUTER JOIN wb_items_per_site i2 ON i1.ips_item_id=i2.ips_item_id AND i2.ips_site_id='$k2' WHERE i1.ips_site_id='$k1' AND i2.ips_site_id IS NULL" ;
	if ( $starts_with != '' ) $sql .= " AND i1.ips_site_page >= '" . get_db_safe ( $starts_with ) . "'" ;
	if ( $cat != '' ) {
		$db2 = openDB ( $lang[1] , $projects[$proj[1]] ) ;
		$pages = getPagesInCategory ( $db2 , ucfirst(trim($cat)) , $depth ) ;
		$p2 = array() ;
		foreach ( $pages AS $k => $v ) $p2[] = str_replace ( '_' , ' ' , get_db_safe ( $v ) ) ;
		$sql .= " AND i1.ips_site_page IN ('" . join ( "','" , $p2 ) . "')" ;
	}
	$sql .= " order by i1.ips_site_page limit " . get_db_safe ( $limit ) . " OFFSET " . get_db_safe ( $start ) ;

	$items = array() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$items[$o->ips_item_id] = "Q".$o->ips_item_id ;
	}
	
	$sql = "SELECT term_entity_id,term_text FROM wb_terms WHERE term_entity_id IN ('" . join ( "','" , array_keys($items) ) . "') AND term_type='label' and term_language='" . get_db_safe(get_content_lang($lang[1])) . "' AND term_entity_type='item'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$items[$o->term_entity_id] = $o->term_text ;
	}

	print "<h3>Results " . ($start+1) . "&ndash;" . ($start+$limit) . "</h3>" ;

	$list = "<ol>" ;
	$cnt = 0 ;
	foreach ( $items AS $k => $v ) {
		$list .= "<li" ;
		if ( $cnt == 0 ) $list .= " value='" . ($start+1) . "'" ;
		$list .= "><a target='_blank' href='//www.wikidata.org/wiki/Q$k'>$v</a></li>" ;
		$cnt++ ;
	}
	$list .= "</ol>" ;

	$last_next = "<div style='margin-bottom:10px'>" ;
	if ( $start > 0 ) $last_next .= "<a href='?lang1={$lang[1]}&proj1={$proj[1]}&lang2={$lang[2]}&proj2={$proj[2]}&cat=$cat&depth=$depth&limit=$limit&starts_with=$starts_with&start=" . ($start-$limit) . "&doit=1'>prev</a>" ;
	else if ( $cnt == $limit || $start > 0 ) $last_next .= "prev" ;
	if ( $cnt == $limit ) $last_next .= " | <a href='?lang1={$lang[1]}&proj1={$proj[1]}&lang2={$lang[2]}&proj2={$proj[2]}&cat=$cat&depth=$depth&limit=$limit&starts_with=$starts_with&start=" . ($start+$limit) . "&doit=1'>next</a>" ;
	$last_next .= "</div>" ;

	print $last_next . $list . $last_next ;
}

print get_common_footer() ;

?>